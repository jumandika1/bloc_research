import 'dart:async';

class Counter {
  StreamController _inputController = StreamController();
  StreamSink get sinkInput => _inputController.sink;

  StreamController _outputController = StreamController();
  StreamSink get sinkOutput => _outputController.sink;

  Stream get output => _outputController.stream;

// Declare variable that would be called
  int _counter = 0;
// Declare initial value
  int get initValue => _counter;

  Counter() {
    _inputController.stream.listen((event) {
      // Counter operators
      if (event == 'increment') {
        _counter++;
      } else {
        _counter--;
      }

// Assign result to variable
      sinkOutput.add(_counter);
    });
  }

  void dispose() {
 //prevent unnecessary renders   
    _inputController.close();
    _outputController.close();
  }
}
