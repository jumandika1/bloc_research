import '../blocs/counter.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // counter instance
  Counter counterBloc = Counter();

  @override
  void dispose() {
    //Call dispose from bloc
    counterBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Counter Bloc'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StreamBuilder(
                  // call initial value
                  initialData: counterBloc.initValue,
                  // call actual value after actions
                  stream: counterBloc.output,
                  builder: (context, snapshot) => Text(
                      // get actual value
                      "Current number: ${snapshot.data}",
                      style: const TextStyle(
                        fontSize: 20,
                      ))),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () {
                      // call counter method and define action as decrement
                      counterBloc.sinkInput.add('decrement');
                    },
                    icon: Icon(Icons.remove),
                  ),
                  IconButton(
                    onPressed: () {
                      // call counter method and define action as increment
                      counterBloc.sinkInput.add('increment');
                    },
                    icon: Icon(Icons.add),
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
